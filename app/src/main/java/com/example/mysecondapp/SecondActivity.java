package com.example.mysecondapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import java.util.Random;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        showRandomNumber();
    }

    private static final String TOTAL_COUNT = "total_count";

    public void showRandomNumber(){

        //Get TextView where number will be displayed
        TextView randomView = (TextView) findViewById(R.id.textview_random);

        //Get heading text view
        TextView headingView = (TextView) findViewById(R.id.textview_label);

        int count = getIntent().getIntExtra(TOTAL_COUNT,0);
        Random random = new Random();
        int randomInt = 0;
        if(count>0){
            randomInt = random.nextInt(count);
        }

        //Display random number
        randomView.setText(Integer.toString(randomInt));

        headingView.setText(getString(R.string.random_heading, count));
    }
}
