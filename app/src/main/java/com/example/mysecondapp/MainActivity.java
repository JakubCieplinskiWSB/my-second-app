package com.example.mysecondapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    private static final String TOTAL_COUNT = "total_count";


    //Show Toast
    //@param view - clicked view
    public void toastMe(View view){
        Toast myToast = Toast.makeText(this, "Hello Toast!", Toast.LENGTH_SHORT);
        myToast.show();
    }

    public void countMe(View view){
        //get text view
        TextView showCountTextView = (TextView) findViewById(R.id.textView);

        //Get the value of text view
       String countString = showCountTextView.getText().toString();

       //Increment
        Integer count = Integer.parseInt(countString);
        count++;

        //And display new value
        showCountTextView.setText(count.toString());
    }

    public void randomMe(View view){
        //Create Intent for second activity
        Intent randomIntent = new Intent(this, SecondActivity.class);

        //Get Text View
        TextView showCountTextView = (TextView) findViewById(R.id.textView);

        //Get Value of current number
        String countString = showCountTextView.getText().toString();

        //Parse to Int
        Integer count = Integer.parseInt(countString);

        //put extra info
        randomIntent.putExtra(TOTAL_COUNT,count);

        //Start the activity
        startActivity(randomIntent);
    }
}
